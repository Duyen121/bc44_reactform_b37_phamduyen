import {
  ADD_STUDENT,
  CHANGE_FORM_VALUE,
  CHANGE_SEARCH_VALUE,
  DELETE_STUDENT,
  EDIT_STUDENT,
  SEARCH_STUDENT,
  SHOW_EDIT_DATA,
} from "../constants/studConstants";

export let changeFormValueAction = (values) => {
  return {
    type: CHANGE_FORM_VALUE,
    payload: values,
  };
};

export let changeSearchValueAction = (inputValue) => {
  return {
    type: CHANGE_SEARCH_VALUE,
    payload: inputValue,
  };
};

export let addStudentAction = (student) => {
  return {
    type: ADD_STUDENT,
    payload: student,
  };
};

export let showEditData = (student) => {
  return {
    type: SHOW_EDIT_DATA,
    payload: student,
  };
};

export let editStudentAction = (student) => {
  return {
    type: EDIT_STUDENT,
    payload: student,
  };
};

export let deleteStudentAction = (student) => {
  return {
    type: DELETE_STUDENT,
    payload: student,
  };
};

export let searchStudentAction = (inputValue) => {
  return {
    type: SEARCH_STUDENT,
    payload: inputValue,
  };
};
