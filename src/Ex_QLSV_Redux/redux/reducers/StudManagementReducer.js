import {
  ADD_STUDENT,
  CHANGE_FORM_VALUE,
  CHANGE_SEARCH_VALUE,
  DELETE_STUDENT,
  EDIT_STUDENT,
  SEARCH_STUDENT,
  SHOW_EDIT_DATA,
} from "../constants/studConstants";

const defaultState = {
  values: { id: "", name: "", phoneNum: "", email: "" },
  errors: {
    id: "",
    name: "",
    phoneNum: "",
    email: "",
  },
  studentArr: [
    {
      id: 1,
      name: "ASD",
      phoneNum: "09876876786",
      email: "mnshs@gmail.com",
    },
  ],
  searchValue: "",
  valid: false,
  isEdit: false,
};

export const StudManagementReducer = (
  state = defaultState,
  { type, payload }
) => {
  switch (type) {
    // setSate for form input value
    case CHANGE_FORM_VALUE: {
      let newValue = { ...state.values, [payload.name]: payload.value };
      let newError = { ...state.errors };
      if (payload.value.trim() === "") {
        newError[payload.name] = "Vui lòng nhập đủ thông tin";
      } else {
        if (payload.pattern) {
          const regex = new RegExp(payload.pattern);
          const isValid = regex.test(payload.value);
          if (!isValid) {
            newError[payload.name] = "Vui lòng nhập thông tin hợp lệ";
          } else {
            newError[payload.name] = "";
          }
        }
      }
      let newValid = true;
      for (let key in newError) {
        if (newError[key] !== "" || newValue[key] === "") {
          newValid = false;
        }
      }

      return { ...state, values: newValue, errors: newError, valid: newValid };
    }
    // change seach input value
    case CHANGE_SEARCH_VALUE: {
      return { ...state, searchValue: payload.value };
    }
    // add student to Table
    case ADD_STUDENT: {
      let newStudArr = [...state.studentArr, payload];
      state.studentArr = newStudArr;
      return { ...state };
    }
    // display editting student info on Form
    case SHOW_EDIT_DATA: {
      let cloneStudArr = [...state.studentArr];
      let index = cloneStudArr.findIndex((item) => {
        return item.id === payload.id;
      });
      let targetStud = cloneStudArr[index];
      return {
        ...state,
        values: targetStud,
        valid: true,
        isEdit: true,
      };
    }
    case EDIT_STUDENT: {
      let cloneStudArr = [...state.studentArr];
      let index = cloneStudArr.findIndex((item) => {
        return item.id === payload.id;
      });

      cloneStudArr[index] = payload;
      console.log(cloneStudArr);
      return {
        ...state,
        studentArr: cloneStudArr,
        values: { id: "", name: "", phoneNum: "", email: "" },
        isEdit: false,
        valid: false,
      };
    }
    // delete student on Table
    case DELETE_STUDENT: {
      let targetStud = state.studentArr.find((item) => {
        return item.id === payload.id;
      });
      if (targetStud !== -1) {
        let newStudArr = [...state.studentArr];
        newStudArr.splice(targetStud, 1);
        return { ...state, studentArr: newStudArr };
      }
    }
    // return search result after submit search
    case SEARCH_STUDENT: {
      let searchValue = payload.toLowerCase().replaceAll(" ", "");
      console.log(searchValue);
      let cloneStudArr = [...state.studentArr];
      let result = cloneStudArr.filter((student) => {
        let studName = student.name.toLowerCase().replaceAll(" ", "");
        return searchValue.length > 0 && studName.includes(searchValue);
      });
      if (result.length >= 1) {
        return { ...state, studentArr: result };
      }
    }
    default: {
      return { ...state };
    }
  }
};
