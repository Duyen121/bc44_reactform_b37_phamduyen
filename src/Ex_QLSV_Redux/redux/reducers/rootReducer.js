import { combineReducers } from "redux";
import { StudManagementReducer } from "./StudManagementReducer";

export const rootReducer = combineReducers({ StudManagementReducer });
