import React, { Component } from "react";
import Form from "./Form";
import Table from "./Table";

export default class Ex_QLSV_Redux extends Component {
  render() {
    return (
      <div className="container">
        <br />
        <h3>React Form - Validation</h3>
        <Form />
        <Table />
      </div>
    );
  }
}
