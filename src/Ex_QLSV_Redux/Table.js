import React, { Component } from "react";
import { connect } from "react-redux";
import {
  changeSearchValueAction,
  deleteStudentAction,
  searchStudentAction,
  showEditData,
} from "./redux/actions/studAction";

class Table extends Component {
  // handleEdit = () => {
  //   const foundProductIndex = this.state.listProducts.findIndex((item) => {
  //     return item.id === this.state.editingProduct.id;
  //   });
  //   const newListProducts = [...this.state.listProducts];
  //   newListProducts[foundProductIndex] = this.state.values;
  //   this.setState({
  //     listProducts: newListProducts,
  //     values: {
  //       id: "",
  //       name: "",
  //       image: "",
  //       type: "Apple",
  //       price: 0,
  //       description: "",
  //     },
  //     editingProduct: null,
  //   });
  // };

  renderStudent = () => {
    let { studArr } = this.props;
    return studArr.map((student) => {
      return (
        <tr key={student.id}>
          <td>{student.id}</td>
          <td>{student.name}</td>
          <td>{student.phoneNum}</td>
          <td>{student.email}</td>
          <td>
            <button
              onClick={() => {
                this.props.handleStartEdit(student);
              }}
              className="btn btn-warning mr-2"
            >
              Sửa
            </button>
            <button
              onClick={() => {
                this.props.handleDelete(student);
              }}
              className="btn btn-danger"
            >
              Xoá
            </button>
          </td>
        </tr>
      );
    });
  };

  render() {
    let { searchValue } = this.props;
    // console.log(searchValue);
    return (
      <div className="container mt-2">
        <form
          onSubmit={(e) => {
            e.preventDefault();
            this.props.handleSearch(searchValue);
          }}
        >
          <div className="input-group search-section mt-3 mb-2">
            <input
              type="search"
              value={searchValue}
              name="search"
              className="form-control"
              placeholder="Họ tên sinh viên"
              onChange={(e) => {
                let { value } = e.target;
                this.props.handelChangeSearchInput({ value });
              }}
            />
            <div className="input-group-append">
              <button
                className="btn btn-outline-secondary text-white bg-dark"
                type="submit"
              >
                Tìm kiếm
              </button>
            </div>
          </div>
        </form>
        <table className="table">
          <thead>
            <tr className="bg-dark text-white">
              <th>Mã SV</th>
              <th>Họ tên</th>
              <th>Số điện thoại</th>
              <th>Email</th>
              <th>Thao tác</th>
            </tr>
          </thead>
          <tbody>{this.renderStudent()}</tbody>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    searchValue: state.StudManagementReducer.searchValue,
    studArr: state.StudManagementReducer.studentArr,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    handleDelete: (student) => {
      dispatch(deleteStudentAction(student));
    },
    handleStartEdit: (student) => {
      dispatch(showEditData(student));
    },
    handelChangeSearchInput: (inputValue) => {
      dispatch(changeSearchValueAction(inputValue));
    },
    handleSearch: (inputValue) => {
      dispatch(searchStudentAction(inputValue));
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Table);
