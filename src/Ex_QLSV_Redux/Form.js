import React, { Component } from "react";
import { connect } from "react-redux";
import {
  addStudentAction,
  changeFormValueAction,
  editStudentAction,
} from "./redux/actions/studAction";

class Form extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    this.props.addStudent(this.props.values);
  };

  render() {
    let { values, errors, valid, isEdit } = this.props;
    return (
      <div className="container">
        <div className="card text-left mt-3">
          <div className="card-header bg-dark text-white">
            <h3>Thông tin sinh viên</h3>
          </div>
          <div className="card-body">
            <form onSubmit={this.handleSubmit}>
              <div className="row">
                <div className="form-group col-6">
                  <span>Mã SV</span>
                  {isEdit ? (
                    <input
                      className="form-control"
                      name="id"
                      type="number"
                      pattern="^[a-zA-Z0-9]*$"
                      value={values.id}
                      disabled
                    />
                  ) : (
                    <input
                      className="form-control"
                      name="id"
                      type="number"
                      pattern="^[a-zA-Z0-9]*$"
                      value={values.id}
                      onChange={(e) => {
                        let { name, value, pattern } = e.target;
                        this.props.handleChange({ name, value, pattern });
                      }}
                    />
                  )}

                  {errors.id && (
                    <span className="text-danger">{errors.id}</span>
                  )}
                </div>
                <div className="form-group col-6">
                  <span>Họ tên</span>
                  <input
                    className="form-control"
                    name="name"
                    type="text"
                    pattern="[A-Za-z ]{1,32}"
                    value={values.name}
                    onChange={(e) => {
                      let { name, value, pattern } = e.target;
                      this.props.handleChange({ name, value, pattern });
                    }}
                  />
                  {errors.name && (
                    <span className="text-danger">{errors.name}</span>
                  )}
                </div>
                <div className="form-group col-6">
                  <span>Số điện thoại</span>
                  <input
                    className="form-control"
                    name="phoneNum"
                    type="text"
                    pattern="^[0-9]*$"
                    value={values.phoneNum}
                    onChange={(e) => {
                      let { name, value, pattern } = e.target;
                      this.props.handleChange({ name, value, pattern });
                    }}
                  />
                  {errors.phoneNum && (
                    <span className="text-danger">{errors.phoneNum}</span>
                  )}
                </div>
                <div className="form-group col-6">
                  <span>Email</span>
                  <input
                    className="form-control"
                    name="email"
                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"
                    value={values.email}
                    onChange={(e) => {
                      let { name, value, pattern } = e.target;
                      this.props.handleChange({ name, value, pattern });
                    }}
                  />
                  {errors.email && (
                    <span className="text-danger">{errors.email}</span>
                  )}
                </div>
                <div className="col-md-12 text-right mt-2">
                  {isEdit ? (
                    valid ? (
                      <button
                        onClick={() => {
                          this.props.editStudent(values);
                        }}
                        className="btn btn-info"
                      >
                        Cập nhật
                      </button>
                    ) : (
                      <button className="btn btn-info" disabled>
                        Cập nhật
                      </button>
                    )
                  ) : valid ? (
                    <button className="btn btn-success" type="submit">
                      Thêm
                    </button>
                  ) : (
                    <button className="btn btn-success" type="submit" disabled>
                      Thêm
                    </button>
                  )}
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    values: state.StudManagementReducer.values,
    errors: state.StudManagementReducer.errors,
    valid: state.StudManagementReducer.valid,
    studArr: state.StudManagementReducer.studentArr,
    isEdit: state.StudManagementReducer.isEdit,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    addStudent: (student) => {
      dispatch(addStudentAction(student));
    },
    editStudent: (student) => {
      dispatch(editStudentAction(student));
    },
    handleChange: (values) => {
      dispatch(changeFormValueAction(values));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Form);
