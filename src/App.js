// import logo from "./logo.svg";
import "./App.css";
import Ex_QLSV_Redux from "./Ex_QLSV_Redux/Ex_QLSV_Redux";

function App() {
  return (
    <div className="App">
      <Ex_QLSV_Redux />
    </div>
  );
}

export default App;
